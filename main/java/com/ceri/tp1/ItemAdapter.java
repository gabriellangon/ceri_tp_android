package com.ceri.tp1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    public String[] countrys;
    private AppCompatActivity activity;

    public ItemAdapter(AppCompatActivity activity,String[] countrys){
        this.countrys=countrys;
        this.activity=activity;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView miniFlag;
        public TextView countryName;

        public ItemViewHolder(View itemView) {
            super(itemView);
            miniFlag=itemView.findViewById(R.id.miniFlag);
            countryName=itemView.findViewById(R.id.countryName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(activity,CountryActivity.class);
            intent.putExtra("selected_country",countryName.getText());
            activity.startActivity(intent);
        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview,parent,false);
        ItemViewHolder ivh = new ItemViewHolder(v);
        return ivh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        String name = countrys[position];
        Country country = CountryList.getCountry(name);
        holder.countryName.setText(name);
        int flagID = activity.getResources().getIdentifier(country.getmImgFile() , "drawable", activity.getPackageName());
        holder.miniFlag.setImageResource(flagID);
    }

    @Override
    public int getItemCount() {
        return countrys.length;
    }


}
