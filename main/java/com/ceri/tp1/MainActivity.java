package com.ceri.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView countryListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,CountryList.getNameArray());
        ItemAdapter adapter = new ItemAdapter(this,CountryList.getNameArray());
        countryListView = findViewById(R.id.countryRecyclerView);
        countryListView.setItemAnimator(new DefaultItemAnimator());

        countryListView.setHasFixedSize(true);
        countryListView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        countryListView.setAdapter(adapter);


    }
/*
    AdapterView.OnItemClickListener listViewListenner = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String countryName = countryListView.getItemAtPosition(position).toString();
            Intent intent = new Intent(MainActivity.this,CountryActivity.class);
            intent.putExtra("selected_country",countryName);
            startActivity(intent);
        }
    };
*/

}
