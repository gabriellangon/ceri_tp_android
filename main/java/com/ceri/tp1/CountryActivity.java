package com.ceri.tp1;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CountryActivity extends AppCompatActivity {

    private TextView lblTitle;
    private EditText textCapital;
    private EditText textLanguage;
    private EditText textCurrency;
    private EditText textPopulation;
    private EditText textArea;
    private ImageView countryFlag;

    private Button btnSave;

    private Country country;
    private String countryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(btnListenner);

        Intent intent = getIntent();
        countryName = intent.getStringExtra("selected_country");
        country = CountryList.getCountry(countryName);

        lblTitle = findViewById(R.id.lblTitle);
        lblTitle.setText(countryName);

        countryFlag = findViewById(R.id.countryFlag);
        int flagID = getResources().getIdentifier(country.getmImgFile() , "drawable", getPackageName());
        countryFlag.setImageResource(flagID);
        textCapital = findViewById(R.id.editCapital);
        textCapital.setText(country.getmCapital());
        textLanguage = findViewById(R.id.editLanguage);
        textLanguage.setText(country.getmLanguage());
        textCurrency = findViewById(R.id.editCurrency);
        textCurrency.setText(country.getmCurrency());
        textPopulation = findViewById(R.id.editPopulation);
        textPopulation.setText(country.getmPopulation()+"");
        textArea = findViewById(R.id.editArea);
        textArea.setText(country.getmArea()+"");

    }

    View.OnClickListener btnListenner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            country.setmCapital(textCapital.getText().toString());
            country.setmLanguage(textLanguage.getText().toString());
            country.setmCurrency(textCurrency.getText().toString());
            country.setmArea(Integer.parseInt(textArea.getText().toString()));
            country.setmPopulation(Integer.parseInt(textPopulation.getText().toString()));

            Toast.makeText(getApplication(),"Modification effectué avec succès",Toast.LENGTH_SHORT).show();
        }
    };
}
